import json
import os
import pickle
import re
import copy
import numpy as np

import torch
# from tqdm import tqdm
# import config


# def compute_class(answer):
#     for key, values in classes.items():
#         if answer in values:
#             return key
#
#     raise ValueError('Answer {} does not belong to a known class'.format(answer))

def build_dictionaries(flavr_dir):
    sentence_dirs = os.path.join(flavr_dir, 'easy_split')
    # if not os.path.exists(sentence_dirs):
    #     os.makedirs(sentence_dirs)

    cached_dictionaries = os.path.join(flavr_dir, 'easy_split', 'FLAVR_build_dictionaries.pkl')
    if os.path.exists(cached_dictionaries):
        print('==> using cached dictionaries:{}'.format(cached_dictionaries))
        print(cached_dictionaries)
        with open (cached_dictionaries, 'rb') as f:
            return pickle.load(f)

    sentence_to_idx = {}
    answer_to_idx = {'False': 0, 'True': 1}
    json_train_filename = os.path.join(flavr_dir, 'easy_split', 'train.json')

    # load json file into a list
    json_data = []
    with open(json_train_filename, "r") as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            json_data.append(json.loads(line))

    for jd in json_data:
        sentence = tokenize(jd['natural_language'])
        for word in sentence:
            if word not in sentence_to_idx:
                #one-based indexing #zero is reserved for padding
                sentence_to_idx[word] = len(sentence_to_idx) + 1

    #adding an unknown token for the word that did not appear in the training set
    sentence_to_idx['unknown'] = len(sentence_to_idx) + 1

    res = (sentence_to_idx, answer_to_idx)
    with open(cached_dictionaries, 'wb') as f:
        pickle.dump(res, f)

    return res


def to_dictionary_indexes(dictionary, sentence):
    """
    Outputs indexes of dictionary corresponding to the words in the sequence. (case-insensitive)
    :param dictionary: which dictionary
    :param sentence:
    :return:
    """
    split = tokenize(sentence)
    # idxs = torch.LongTensor([dictionary[w] for w in split])

    sentence_idx = []
    for w in split:
        if w in dictionary:
            sentence_idx.append(dictionary[w])
        else:
            sentence_idx.append(dictionary['unknown'])

    idxs = torch.LongTensor(torch.from_numpy(np.array(sentence_idx)))

    return idxs

def collate_samples_image(batch):
    return collate_samples(batch, False)

def collate_samples_state_description(batch):
    return collate_samples(batch, True)

def collate_samples(batch, state_description):
    """
    Used by DatasetLoader to merge together multiple samples into one mini-batch
    :param batch:
    :param state_description:
    :return:
    """
    images = [d['image'] for d in batch]
    sentences = [d['sentence'] for d in batch]
    answers = [d['answer'] for d in batch]

    #questions are padded to a fixed length (the maximum length in questions)
    #so they can be inserted in a tensor
    batch_size = len(batch)
    max_len = max(map(len, sentences))

    padded_sentences = torch.LongTensor(batch_size, max_len).zero_()
    for i, q in enumerate(sentences):
        padded_sentences[i, :len(q)] = q

    collate_batch = dict(
        image=torch.stack(images),
        answer=torch.stack(answers),
        sentence=torch.stack(padded_sentences)
    )

    return collate_batch


def tokenize(sentence):
    s = re.sub('([.,;:!?()])', r' \1 ', sentence)
    s = re.sub('\s{2,}', ' ', s)

    return [w.lower() for w in s.split()]


def load_tensor_data(data_batch, cuda, invert_sentences, volatile=False):
    # prepare input
    var_kwargs = dict(volatile=True) if volatile else dict(requires_grad=False)

    sentence = data_batch['sentence']
    if invert_sentences:
        # invert question indexes in this batch
        sentence_len = sentence.size()[1]
        sentence = sentence.index_select(1, torch.arange(sentence_len - 1, -1, -1).long())

    img = torch.autograd.Variable(data_batch['image'], **var_kwargs)
    sentence = torch.autograd.Variable(sentence, **var_kwargs)
    label = torch.autograd.Variable(data_batch['answer'], **var_kwargs)
    if cuda:
        img, sentence, label = img.cuda(), sentence.cuda(), label.cuda()

    # label = (label-1).squeeze(1)
    label = (label).squeeze(1)
    return img, sentence, label

def json_transformer(flavr_dir):
    """
    The original JSON file for FLAVR is aggregated in one json file (examples.json).
    The id of each easy split is given in the each of the .txt file inside the folder flavr/data/easy_split.
    The function first loads ids for all the split and segment json data according to the split.


    Then, for data in each split, every entry corresponds to 6 different permutation of the image. Therefore, it takes
    each json entry and add a permutation index to it, then writes the split json data to a new json file.
    For each json file, there is split_id * 6 entries.

    :param flavr_dir: Root directory of FLAVR dataset
    :return: Filename of the 3 new JSON file
    """
    easy_split_dir = os.path.join(flavr_dir, 'easy_split')

    #loads all ids for each split
    train_id = get_split_id(easy_split_dir, 'train')
    dev_id = get_split_id(easy_split_dir, 'dev')
    test_id = get_split_id(easy_split_dir, 'test')
    # print(len(dev_id))
    # print(len(test_id))

    #loads all json entries from data/examples.json
    examples_json_filenname = os.path.join(flavr_dir, 'examples.json')
    json_data = []
    with open(examples_json_filenname) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            json_data.append(json.loads(line))
    # print(json_data[0])
    # print(type(json_data[0]))
    train_data, dev_data, test_data = [], [], []
    for data in json_data:
        if data['identifier'] in train_id:
            train_data.append(data)
        elif data['identifier'] in dev_id:
            dev_data.append(data)
        elif data['identifier'] in test_id:
            test_data.append(data)
        #throw exception if any identifier doesn't exit in examples.json

    #create new json file for each split with permutation added to it
    #total json entry = split * 6 (as there is 6 permutation for each language statement)
    train_json_filename = json_transformer_6x(easy_split_dir, 'train', train_data)
    dev_json_filename = json_transformer_6x(easy_split_dir, 'dev', dev_data)
    test_json_filename = json_transformer_6x(easy_split_dir, 'test', test_data)

def get_split_id(dir, split):
    id_filename = os.path.join(dir, split + '.txt')
    ids = []

    with open(id_filename) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            ids.append(line)

    return set(ids)

def json_transformer_6x(dir, split, json_data):
    split_json_filename = os.path.join(dir, split + '.json')

    if not os.path.exists(split_json_filename):
        num_of_permute = 6

        with open(split_json_filename, 'w') as f:
            for data in json_data:
                for i in range(num_of_permute):
                    new_data = copy.deepcopy(data)
                    new_data['permutation'] = str(i)
                    line = json.dumps(new_data)
                    f.write(str(line))
                    f.write('\n')

    return split_json_filename

# test json_trasformer
# print(json_transformer('/Users/iriszhang/CornellTech/flavr/data'))
