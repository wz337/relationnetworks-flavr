import json
import os
import pickle
from PIL import Image

from collections import Counter
from torch.utils.data import Dataset

import utils
# import torch
# import cv2
# import numpy as np

class FlavrDatasetImages(Dataset):
    """
    Loads image from the FLAVR dataset
    """

    def __init__(self, flavr_dir, transform=None):
        """
        :param flavr_dir: Root directory of FLAVR dataset
        :param split: Specifies which sub-dataset we want to read, train, test, or val
        :param transform: Optional transform to be applied on a sample
        """
        self.img_dir = os.path.join(flavr_dir, 'pngs')
        self.transform = transform

    def __len__(self):
        return len(os.listdir(self.img_dir))

    def __getitem__(self, dir, img_idx, idx, permute_idx):
        img_filename = os.path.join(self.img_dir, str(dir), str(img_idx) + '-' + str(idx) + '-' + str(permute_idx) + '.png')
        image = Image.open(img_filename).convert('RGB')

        if self.transform:
            image = self.transform

        return image

# check if path is correct
# c = FlavrDatasetImages('/Users/iriszhang/CornellTech/flavr/data/')
# img = (c.__getitem__(0, 1, 0, 0))
# print(type(img))
# cv2.imshow('sample', np.array(img))
# cv2.waitKey(0)

class FlavrDataset(Dataset):
    def __init__(self, flavr_dir, split, dictionaries, transform=None):
        """
        :param flavr_dir: root directory
        :param train (boolean): Specifies which sub-dataset we want to read. train=True, val=False
        :param dictionaries: ***
        :param transform: Optional transform to be applied on a sample
        """
        self.split = split
        sentence_json_filename = os.path.join(flavr_dir, 'easy_split', self.split + '.json')
        self.img_dir = os.path.join(flavr_dir, 'pngs')

        # cached_sentences = sentence_json_filename.replace('_6x.json', '-sample.pkl')
        cached_sentences = sentence_json_filename.replace('.json', '.pkl')
        if os.path.exists(cached_sentences):
            print('==>using cached sentences: {}'.format(cached_sentences))
            with open(cached_sentences, 'rb') as f:
                self.sentences = pickle.load(f)
            # print(len(self.sentences))
        else:
            # load json file into a list
            self.sentences = []
            with open(sentence_json_filename, "r") as f:
                lines = f.readlines()
                for line in lines:
                    line = line.strip()
                    self.sentences.append(json.loads(line))
            with open(cached_sentences, 'wb') as f:
                pickle.dump(self.sentences, f)

        self.flavr_dir = flavr_dir
        self.transform = transform
        self.dictionaries = dictionaries

    def answer_weights(self):
        n = float(len(self.sentences))
        answer_count = Counter(q['label'].lower() for q in self.sentences)
        weights = [n/answer_count[q['label'].lower()] for q in self.sentences]
        return weights

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, idx):
        current_sentence = self.sentences[idx]
        img_filename = os.path.join(self.img_dir, current_sentence['path'][7:], current_sentence['identifier'] + '-' +
                               current_sentence['permutation'] + '.png')
        image = Image.open(img_filename).convert('RGB')

        # self.dictionaries = utils.build_dictionaries('/Users/iriszhang/CornellTech/flavr/data/')
        sentence = utils.to_dictionary_indexes(self.dictionaries[0], current_sentence['natural_language'])
        answer = utils.to_dictionary_indexes(self.dictionaries[1], current_sentence['label'])

        sample = {'image': image, 'sentence': sentence, 'answer': answer}

        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample


# check item
# c = FlavrDataset('/Users/iriszhang/CornellTech/flavr/data/', 'train', "", "")
# print (c.__getitem__(0))

